import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SupportServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletContext sc = this.getServletContext();

        resp.getWriter().println("Thank you! " + req.getParameter("name") +
                " for contacting us. We should receive reply from us with in 24 hrs in your email address " +
                req.getParameter("email") + ". Let us know in our support email " + sc.getInitParameter("support-email") +
                " if you don't receive reply within 24 hrs. Please be sure to attach your reference " + (int)(Math.random() * 10000 + 100000) + " in your email.");
    }
}
